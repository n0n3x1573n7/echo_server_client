from tkinter import *
import threading, time
from socket import *
from sys import argv

IP_SERVER = None
PORT = None
BUFFER_SIZE = 1024

###### shared memory #####
client_socket = None     #
lock = threading.Lock()  #
##########################


class Thread_for_Recv(threading.Thread):
    def __init__(self, listbox):
        threading.Thread.__init__(self)
        self.listbox = listbox
        
    def run(self):        
        while True:
            # Receive messages from server
            with lock:
                try:
                    data = client_socket.recv(BUFFER_SIZE)
                except:
                    data = ""
            if data:
                self.listbox.insert(END, data)
                self.listbox.yview(END)


class Thread_for_Send(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        # Send the greeting message to server
        
        while True:
            # Read message from keyboard
            data = input("Enter message: ")

            # Send the message to server
            with lock:
                client_socket.sendall(("{}".format(data)).encode())

def usage():
    print("syntax : echo_client <host> <port>")
    print("sample : echo_client 127.0.0.1 1234")

def main():
    global IP_SERVER, PORT, client_socket
    try:
        print(argv)
        name, IP_SERVER, PORT = argv
        PORT=int(PORT)
        client_socket = socket(AF_INET, SOCK_STREAM)
        client_socket.connect((IP_SERVER,PORT))
    except:
        usage()
        exit()

    root = Tk()
    root.geometry("400x350")
    root.title("Message Window")
    
    frame = Frame(root, width = 300, height = 400, bd=1)
    frame.pack()

    scrollbar = Scrollbar(frame)
    scrollbar.pack(side = RIGHT, fill = Y)
    listbox = Listbox(frame, width = 100, height=20)
    listbox.pack(side=LEFT,fill=Y) 

    listbox.configure(yscrollcommand = scrollbar.set)
    scrollbar.configure(command = listbox.yview)

    print("[Client] connected to server")
    client_socket.settimeout(0.0)  # nonblocking recv
    
    Thread_for_Recv(listbox).start()
    Thread_for_Send().start()
    
    root.mainloop()
    client_socket.close()
    
if __name__ == '__main__':
    main()