import threading, time
from socket import *
from sys import argv

PORT = None
BOPT = False
BUFFER_SIZE = 1024

###### shared memory #####
client_count = 0         #
server_sockets = {}      #
msg_queue = []           #
lock = threading.Lock()  #
##########################


# Thread that broadcasts data to every client
class Thread_for_Broadcasting(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run(self):
        global server_sockets
        global msg_queue
        while True:
            time.sleep(0.5)
            with lock:
                print(msg_queue, server_sockets.keys())
                while len(msg_queue)!=0:
                    target, data = msg_queue.pop(0)
                    try:
                        sent = server_sockets[target].send(data)
                        print('sent {} to {}'.format(data, target))
                    except Exception as e:
                        print(e)

# Thread that communicates with each client
class Thread_for_Communication(threading.Thread):
    def __init__(self, username, server_socket):
        threading.Thread.__init__(self)
        self.username = username
        self.server_socket = server_socket
        
    def run(self):
        global server_sockets
        global msg_queue
        while True:
            # Receive message that client sends
            # (blocked until data is received)
            try:
                data = self.server_socket.recv(BUFFER_SIZE)
            except:
                data = b""
            if data == b"":
                self.server_socket.close()
                with lock:
                    del server_sockets[self.username]
                print("[Server] socket {} broken".format(self.username))
                with lock:
                    print("[Server] # remaining clients:", len(server_sockets))
                return
                
            print("[Server] received data {} from client {}".format(data, self.username))

            with lock:
                parse(self.username, data)

def parse(sender, data):
    print('[Server] parsing data {} from client {}'.format(data, sender))
    if BOPT:
        for name in server_sockets:
            msg_queue.append((name, data))
    else:
        msg_queue.append((sender, data))

def usage():
    print('syntax : echo_server <port> [-b]')
    print('sample : echo_server 1234 -b')

# Main threads listens for connection requests
def main():
    global client_count, PORT, BOPT
    try:
        print(argv)
        if len(argv)==2:
            name, PORT = argv
        elif len(argv)==3:
            name, PORT, BOPT = argv
            BOPT = (BOPT == '-b')
        else:
            raise Exception
        PORT=int(PORT)
    except:
        usage()
        exit()

    Thread_for_Broadcasting().start()

    socket_for_connection = socket(AF_INET, SOCK_STREAM)
    socket_for_connection.bind(("", PORT))
    socket_for_connection.listen(1)
    print("[Server] listen for connection requests")

    while True:
        server_socket, dummy = socket_for_connection.accept()
        print("[Server] accept connection from client", dummy)
        username=str(client_count+1)
        client_count+=1
        
        with lock:
            server_sockets[username]=server_socket
        
        thread_for_communication = Thread_for_Communication(username, server_socket)
        thread_for_communication.start()

if __name__ == '__main__':
    main()
